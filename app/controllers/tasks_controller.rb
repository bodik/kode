class TasksController < ApplicationController
	def task
		require 'net/http'
		require 'base_processor'
		require 'exequter'
		require 'single_proc'
		require 'hard_proc'
		require 'double_proc'
		
		task = params[:id]
		url = URI.parse("http://kodaday.intita.com/api/task/#{task}")
		http = Net::HTTP.new(url.host, url.port)
		req = Net::HTTP::Get.new(url.request_uri)
		req["X-API-KEY"] = '8A57dHfg'
		response = JSON.parse http.request(req).body
		@processors = response['processors']
		@tasks = response['tasks']
		
		if params[:id].to_i == 1
			processor_hashs = Exequter.make_for_first(@processors)
		elsif params[:id].to_i == 4
			processor_hashs = Exequter.make_for_four(@processors)
		elsif params[:id].to_i == 5
			processor_hashs = Exequter.make_for_five(@processors)
		end
		
		@new_group = []
		@tasks.each_with_index do |task, index|
			hash = {}
			hash['index'] = index
			hash['value'] = task
			if params[:id].to_i == 1
				hash['group'] = BaseProcessor.group(task)
			elsif params[:id].to_i == 4
				hash['group'] = BaseProcessor.group_four(task)
			elsif params[:id].to_i == 5
				hash['group'] = BaseProcessor.group_five(task)
			end
			@new_group.push(hash)
		end

		@respons = []
		@new_group.each do |task|
			result = []
			processor_hashs.map{|k, v| k if k['type'] == task['group']}.compact.each do |item|
				result.push(BaseProcessor.exequte_by_type(item['type'], task['value'], item))
			end

			result = result.sort_by(&:last)
			min = result[0]
			
			processor_hashs.each do |item|
				if item['index'] == min[0]
					item['work'] = item['work'] + min[1]
					item['cpu'] = item['cpu'] + min[1]
				end
			end

			@respons.push(min[0])
			
			processor_hashs.each do |item|
				if item['cpu'] > 0
					item['cpu'] = item['cpu'] - 1
				end
			end
		end
		
		# @arr = []
		# @width = 0
		# @tasks.each_with_index do |task|
		# 	if task%13 == 0
		# 		@arr.push(@width)
		# 		@width = -1
		# 	end
		# 	@width += 1
		# end
		
		uri = URI("http://kodaday.intita.com/api/task/#{task}")
		req = Net::HTTP::Post.new(uri)
		req["X-API-KEY"] = '8A57dHfg'
		req.body =@respons.to_json
		res = Net::HTTP.start(uri.hostname, uri.port) do |http|
			http.request(req).body
		end

		@res = res
		@task = task
	end
end