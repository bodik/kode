class SingleProc < BaseProcessor
	
	def self.result(task, proc)
		if proc['no'] == 0
			[proc['index'], task, proc['cpu']]
		else
			[proc['index'], 3 * task + 1, proc['cpu']]
		end
	end
end