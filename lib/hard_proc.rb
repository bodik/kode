class HardProc < BaseProcessor
	def self.result(task, proc)
		if task%13 == 0
			[proc['index'], task / 13, proc['cpu']]
		else
			[proc['index'], 3 * task + 1, proc['cpu']]
		end
	end
end