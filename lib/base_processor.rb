class BaseProcessor
	
	def self.type(processor)
		type = ''
		if processor == 0
			type = 'single_proc'
		elsif processor == 1
			type = 'hard_proc'
		elsif processor == 2
			type = 'double_proc'
		end
		type
	end
	
	def self.exequte_by_type(type, task, proc)
		require 'single_proc'
		require 'hard_proc'
		require 'double_proc'
		if type == 'simple'
			SingleProc.result(task, proc)
		elsif type == 'third'
			HardProc.result(task, proc)
		elsif type == 'double'
			DoubleProc.result(task, proc)
			# SingleProc.result(task, proc)
		end
	end
	
	def self.group(task)
		if (task%2 == 0 && task%13 != 0)
			'double'
		elsif task%13 == 0
			'third'
		else
			'simple'
		end
	end
	
	def self.group_five(task)
		if (task%2 == 0 && task%13 != 0)
			'simple'
		elsif task%13 == 0
			'third'
		else
			'simple'
		end
	end
	
	def self.group_four(task)
		if task%2 == 0 && task%13 != 0
			'double'
		else
			'third'
		end
	end
end