class Exequter
	
	def self.make_proc_arr(arr_proc)
		require 'base_processor'
		arr = []
		arr_proc.each_with_index do |proc, index|
			hash = {}
			hash['index'] = index
			hash['cpu'] = 0
			hash['type'] = BaseProcessor.type(proc)
			hash['work'] = 0
			arr.push(hash)
		end
		arr
	end
	
	def self.make_for_first(arr_proc)
		require 'base_processor'
		arr = []
		arr_proc.each_with_index do |proc, index|
			hash = {}
			hash['index'] = index
			hash['cpu'] = 0
			if index == 0 || index == 2 || index == 5 
				hash['type'] = 'simple'
			elsif index == 1
				hash['type'] = 'third'
			elsif index == 3 || index == 4
				hash['type'] = 'double'
			end
			hash['no'] = proc
			hash['work'] = 0
			arr.push(hash)
		end
		arr
	end
	
	def self.make_for_five(arr_proc)
		require 'base_processor'
		arr = []
		arr_proc.each_with_index do |proc, index|
			hash = {}
			hash['index'] = index
			hash['cpu'] = 0
			if index == 0
				hash['type'] = 'third'
			else
				hash['type'] = 'simple'
			end
			hash['no'] = proc
			hash['work'] = 0
			arr.push(hash)
		end
		arr
	end

	def self.make_for_four(arr_proc)
		require 'base_processor'
		arr = []
		arr_proc.each_with_index do |proc, index|
			hash = {}
			hash['index'] = index
			hash['cpu'] = 0
			if index == 0
				hash['type'] = 'double'
			else
				hash['type'] = 'third'
			end
			hash['no'] = proc
			hash['work'] = 0
			arr.push(hash)
		end
		arr
	end
end